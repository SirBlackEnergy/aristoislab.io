const fs = require('fs');

const html = "./public";
const capes = `${html}/capes`;

function generateCapeIndex() {
    let json = {};
    const index = `${capes}/index.json`;
    for (let file of fs.readdirSync(capes)) {
        json[file.substring(0, file.length - ".png".length)] = `https://assets.aristois.net/capes/${file}`;
    }
    fs.writeFileSync(index, JSON.stringify(json));
}

generateCapeIndex();
